<?php
define( 'WP_CACHE', true );
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', "wp_local_ac" );

/** MySQL database username */
define( 'DB_USER', "root" );

/** MySQL database password */
define( 'DB_PASSWORD', "root" );

/** MySQL hostname */
define( 'DB_HOST', "localhost" );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          '_$Q-U_*^5T{D[|L-<IVu12 ut!YK]ceVz}B|-3gF6]g0w~Ps68jzP+%njS`z&bIT' );
define( 'SECURE_AUTH_KEY',   '#Xn>Y2rwk(0X5Ce?k_d<-!B|p04zxf1H~ zaUMsG[<>8hi#M+W)Z^sGX+5UoB[=~' );
define( 'LOGGED_IN_KEY',     '9OUT*+G>(6T#VDR>5&Cf*,c_t9ig)6L;tp&OZd_n(Z|3!~BW!:k&]?D!L5fgpvFf' );
define( 'NONCE_KEY',         'KBcbIVuVI)@(~Z:P($Xgs9U:,XysiW{RF:ND)$wtYC1>MC/D-;jnzhCHY7c;Nf=o' );
define( 'AUTH_SALT',         '7kDA0q!>u5A~AD1yA!0m@!OE-]C/S].rL#8t&C kToRSd|%$(L|!4)^i#*iZ3!tC' );
define( 'SECURE_AUTH_SALT',  'uI4JPjz)&$@2n7cx+R(1 |l4LC!suawCO]bM4rh`p(eTg$<v#W#V)#f2w,Fq_+#0' );
define( 'LOGGED_IN_SALT',    'ZxzACnH<Z#/1xD.|D%ptiK} a8@qLm;_?qbq<)UM2$_a26dK[161EF^u%Xx|lxcW' );
define( 'NONCE_SALT',        '^6Ce>R)}X@3Tv[5ncIgD}E<QtCCk4rzm;wP`eCaZ6ugMfPrdP!]9?>uJ!f;{CG7a' );
define( 'WP_CACHE_KEY_SALT', '^!]+[=[ bT~e5}K`P`7}E_kC:~kL@4$HWiMyg(XnMCr`fK8.m^Tt-w{7R{{&O_-L' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




define( 'WP_AUTO_UPDATE_CORE', true );
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
